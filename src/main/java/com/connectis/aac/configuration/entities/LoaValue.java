package com.connectis.aac.configuration.entities;

public enum LoaValue {
    PASSWORD, SECOND_FACTOR
}
