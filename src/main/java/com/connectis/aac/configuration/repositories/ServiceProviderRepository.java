package com.connectis.aac.configuration.repositories;

import com.connectis.aac.configuration.entities.ServiceProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "serviceProvider", path = "service-provider")
public interface ServiceProviderRepository extends CrudRepository<ServiceProvider, UUID> {
}
