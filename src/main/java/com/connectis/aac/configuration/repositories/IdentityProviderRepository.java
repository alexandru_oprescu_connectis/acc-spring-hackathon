package com.connectis.aac.configuration.repositories;

import com.connectis.aac.configuration.entities.IdentityProvider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "identityProvider", path = "identity-provider")
public interface IdentityProviderRepository extends CrudRepository<IdentityProvider, UUID> {
}
