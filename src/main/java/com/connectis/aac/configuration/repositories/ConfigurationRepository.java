package com.connectis.aac.configuration.repositories;

import com.connectis.aac.configuration.entities.Configuration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "configuration", path = "configuration")
public interface ConfigurationRepository extends CrudRepository<Configuration, UUID> {
}
