package com.connectis.aac.configuration.repositories;


import com.connectis.aac.configuration.entities.Rule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "rule", path = "rule")
public interface RuleRepository extends CrudRepository<Rule, UUID> {
}
