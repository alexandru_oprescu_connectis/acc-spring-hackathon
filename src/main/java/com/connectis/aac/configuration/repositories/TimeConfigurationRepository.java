package com.connectis.aac.configuration.repositories;

import com.connectis.aac.configuration.entities.TimeInterval;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "timeConfiguration", path = "time-configuration")
public interface TimeConfigurationRepository extends CrudRepository<TimeInterval, UUID> {
}
