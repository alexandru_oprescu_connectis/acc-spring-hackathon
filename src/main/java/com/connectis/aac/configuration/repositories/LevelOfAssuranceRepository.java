package com.connectis.aac.configuration.repositories;

import com.connectis.aac.configuration.entities.LevelOfAssurance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "levelOfAssurance", path = "levelOfAssurance")
public interface LevelOfAssuranceRepository extends CrudRepository<LevelOfAssurance, UUID> {
}
