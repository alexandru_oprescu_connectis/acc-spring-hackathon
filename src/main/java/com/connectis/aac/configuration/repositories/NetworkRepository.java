package com.connectis.aac.configuration.repositories;

import com.connectis.aac.configuration.entities.Network;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "network", path = "network")
public interface NetworkRepository extends CrudRepository<Network, UUID> {
}
