package com.connectis.aac.controllers;

import com.connectis.aac.security.AuthenticatorProvider;
import com.connectis.aac.security.context.ContextProvider;
import com.connectis.aac.security.context.LoginContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class BrokerController {

    @Autowired
    private ContextProvider contextProvider;

    @Autowired
    private AuthenticatorProvider authenticatorProvider;

    public static String TWO_FACTOR = "2FA";
    public static String IDP = "IDP";

    private static Map<String, List<String>> idps;

    @PostMapping("/broker")
    public String main(Model model, HttpServletRequest httpServletRequest) {
        idps = new HashMap<>(authenticatorProvider.getIdps(contextProvider.getLoginContext(httpServletRequest)));

        if(!idps.isEmpty() && idps.get(IDP).contains(IDP)) {
            idps.get(IDP).remove(IDP);
            return "IdpSimulator";
        } else {
            return "Error";
        }
    }

    @PostMapping("/broker/continue")
    public String twoFactor(Model model) {
        if(!idps.isEmpty() && idps.get(IDP).contains(TWO_FACTOR)) {
            idps.remove(IDP);
            return "2FA";
        } else {
            return "Success";
        }
    }
}
