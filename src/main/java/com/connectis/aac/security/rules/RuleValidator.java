package com.connectis.aac.security.rules;

import com.connectis.aac.configuration.entities.Rule;
import com.connectis.aac.security.context.LoginContext;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class RuleValidator {

    public ValidationResult validate(LoginContext loginContext, Rule rule) {
        if(loginContext.getNetwork().getValue().equals(rule.getConfiguration().get(0).getNetwork().getValue())) {
            return ValidationResult.FAIL;
        } else {
            return ValidationResult.fromLoa(rule.getConfiguration().get(0).getLevelOfAssurance().getValue());
        }
    }
}
