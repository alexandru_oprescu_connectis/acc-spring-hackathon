package com.connectis.aac.security.rules;

import com.connectis.aac.configuration.entities.LoaValue;

public enum ValidationResult {
    OK, FAIL, SECOND_FACTOR;

    public static ValidationResult fromLoa(LoaValue loa) {
        if(loa.equals(LoaValue.PASSWORD)) {
            return OK;
        } else if(loa.equals(LoaValue.SECOND_FACTOR)) {
            return SECOND_FACTOR;
        } else {
            return FAIL;
        }
    }
}
