package com.connectis.aac.security.context;

import com.connectis.aac.configuration.entities.Device;
import com.connectis.aac.configuration.entities.LevelOfAssurance;
import com.connectis.aac.configuration.entities.Location;
import com.connectis.aac.configuration.entities.Network;
import com.connectis.aac.configuration.entities.TimeInterval;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class LoginContext {
    private Location location;
    private Network network;
    private Device device;
    private TimeInterval timeInterval;
    private LevelOfAssurance levelOfAssurance;
}
