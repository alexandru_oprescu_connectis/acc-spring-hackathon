package com.connectis.aac.security.context;

import com.connectis.aac.configuration.entities.Network;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class ContextProvider {
    public LoginContext getLoginContext(HttpServletRequest httpServletRequest) {
        LoginContext loginContext = new LoginContext();
        Network network = new Network();
        network.setValue(httpServletRequest.getRemoteAddr());
        loginContext.setNetwork(network);
        return loginContext;
    }
}
