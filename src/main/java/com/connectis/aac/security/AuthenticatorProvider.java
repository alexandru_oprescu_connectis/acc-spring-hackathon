package com.connectis.aac.security;

import com.connectis.aac.configuration.entities.Rule;
import com.connectis.aac.configuration.repositories.RuleRepository;
import com.connectis.aac.controllers.BrokerController;
import com.connectis.aac.security.context.ContextProvider;
import com.connectis.aac.security.context.LoginContext;
import com.connectis.aac.security.rules.RuleValidator;
import com.connectis.aac.security.rules.ValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class AuthenticatorProvider {

    @Autowired
    private RuleValidator ruleValidator;

    @Autowired
    private RuleRepository ruleRepository;

    public Map<String, List<String>> getIdps(LoginContext loginContext) {

//        LoginContext loginContext = contextProvider.getLoginContext();

        Iterable<Rule> rules = ruleRepository.findAll();  //().filter(rule-> isForSP());
        for(Rule rule : rules) {
            if(ValidationResult.OK.equals(ruleValidator.validate(loginContext, rule))) {
                return Collections.emptyMap();
            }
        }

        ArrayList<String> listIDPS = new ArrayList<String>();
        listIDPS.add(BrokerController.IDP);
        listIDPS.add(BrokerController.TWO_FACTOR);
        return Map.of(BrokerController.IDP, listIDPS);
    }
}
