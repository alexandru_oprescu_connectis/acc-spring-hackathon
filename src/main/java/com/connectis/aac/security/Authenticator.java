package com.connectis.aac.security;

import com.connectis.aac.security.context.LoginContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

public class Authenticator {

    @Autowired
    private AuthenticatorProvider authenticatorProvider;

    public void login(LoginContext loginContext) {
        Map<String, List<String>> idps = authenticatorProvider.getIdps(loginContext);
    }
}
