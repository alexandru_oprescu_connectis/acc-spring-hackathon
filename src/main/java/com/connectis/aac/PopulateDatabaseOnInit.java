package com.connectis.aac;

import com.connectis.aac.configuration.entities.Configuration;
import com.connectis.aac.configuration.entities.Device;
import com.connectis.aac.configuration.entities.IdentityProvider;
import com.connectis.aac.configuration.entities.LevelOfAssurance;
import com.connectis.aac.configuration.entities.LoaValue;
import com.connectis.aac.configuration.entities.Location;
import com.connectis.aac.configuration.entities.Network;
import com.connectis.aac.configuration.entities.Rule;
import com.connectis.aac.configuration.entities.ServiceProvider;
import com.connectis.aac.configuration.entities.TimeInterval;
import com.connectis.aac.configuration.repositories.ConfigurationRepository;
import com.connectis.aac.configuration.repositories.DeviceRepository;
import com.connectis.aac.configuration.repositories.IdentityProviderRepository;
import com.connectis.aac.configuration.repositories.LevelOfAssuranceRepository;
import com.connectis.aac.configuration.repositories.LocationRepository;
import com.connectis.aac.configuration.repositories.NetworkRepository;
import com.connectis.aac.configuration.repositories.RuleRepository;
import com.connectis.aac.configuration.repositories.ServiceProviderRepository;
import com.connectis.aac.configuration.repositories.TimeConfigurationRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

@Component
public class PopulateDatabaseOnInit implements InitializingBean {

    @Autowired
    private ConfigurationRepository configurationRepository;
    @Autowired
    private DeviceRepository deviceRepository;
    @Autowired
    private LevelOfAssuranceRepository levelOfAssuranceRepository;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private NetworkRepository networkRepository;
    @Autowired
    private ServiceProviderRepository serviceProviderRepository;
    @Autowired
    private TimeConfigurationRepository timeConfigurationRepository;
    @Autowired
    private RuleRepository ruleRepository;
    @Autowired
    private IdentityProviderRepository identityProviderRepository;

    @Override
    public void afterPropertiesSet() throws Exception {

        Device device = new Device();
        device.setValue("Device 1");
        device = this.deviceRepository.save(device);

        LevelOfAssurance levelOfAssurance = new LevelOfAssurance();
        levelOfAssurance.setValue(LoaValue.PASSWORD);
        levelOfAssurance = this.levelOfAssuranceRepository.save(levelOfAssurance);

        Location location = new Location();
        location.setValue("office");
        location = this.locationRepository.save(location);

        Network network = new Network();
        network.setValue("192.168.10.1");
        network = this.networkRepository.save(network);

        ServiceProvider serviceProvider = new ServiceProvider();
        serviceProvider.setValue("SP1");
        serviceProvider = this.serviceProviderRepository.save(serviceProvider);

        TimeInterval timeConfiguration = new TimeInterval();
        timeConfiguration.setStart(LocalDateTime.now().minusHours(4));
        timeConfiguration.setEnd(LocalDateTime.now().plusHours(4));
        timeConfiguration = this.timeConfigurationRepository.save(timeConfiguration);

        Configuration configuration = new Configuration();
        configuration.setDevice(device);
        configuration.setLevelOfAssurance(levelOfAssurance);
        configuration.setLocation(location);
        configuration.setNetwork(network);
        configuration.setServiceProvider(serviceProvider);
        configuration.setTimeConfiguration(timeConfiguration);
        configuration = this.configurationRepository.save(configuration);

        Rule rule = new Rule();
        rule.setName("Rule nr 1");
        rule.setDescription("You do not talk about Fight Club");
        rule.setConfiguration(Collections.singletonList(configuration));
        rule = this.ruleRepository.save(rule);

        IdentityProvider identityProvider = new IdentityProvider();
        identityProvider.setValue("IDP1");
        identityProvider = this.identityProviderRepository.save(identityProvider);
    }
}